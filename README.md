EP1 - OO

Orientação a Objetos 1/2018

Rafaella O. de F. Junqueira - 16/0142628 

Descriptografando mensagens em imagens.

Este projeto consiste em um programa em C++ capaz de abrir, copiar, descriptografar e mostrar uma mensagem.

O objetivo do programa é mostrar ao usuário a imagem escondida nas imagens.

OBSERVAÇÃO

É preciso que a imagem a ser analisada seja copiada para a pasta do projeto.


COMO COMPILAR

- Para executar este programa é necessário copiar as imagens que serão testadas para dentro da pasta raiz (EP1_OO);
- Digite "make clean" Para limpar os arquivos objetos;
- Digite "make" Para compilar;
- Digite "make run" para rodar o programa; 
