#include "imagem.hpp"
#include <iostream> 
#include <fstream>
#include <string>

using namespace std;

Imagem::Imagem(){}

Imagem::Imagem(string filename){
	this -> setArquivo(filename);
}
Imagem::~Imagem(){
	arquivo.close();
}
void Imagem::setArquivo(string filename){
	this -> arquivo.open(filename);
}
ifstream& Imagem::getArquivo(){
	return arquivo;
}
void Imagem::setTipo(string tipo){
	this -> tipo = tipo;
}
string Imagem::getTipo(){
	return tipo;
}
void Imagem::setLargura(int largura){
	this -> largura = largura;	
}
int Imagem::getLargura(){
	return largura;
}
void Imagem::setAltura(int altura){
	this -> altura = altura;
}
int Imagem::getAltura(){
	return altura;
}
void Imagem::setComentario(string comentario){
	this -> comentario = comentario;
}
string Imagem::getComentario(){
	return comentario;
}
void Imagem::setMaximo(int maximo){
	this -> maximo = maximo;
}
int Imagem::getMaximo(){
	return maximo;
}
void Imagem::setInfo(string info){
	this -> info = info; 
}
string Imagem::getInfo(){
	return info;
}
void Imagem::leitura_cabecalho(){

	if(arquivo){

		getline(arquivo, tipo);
		arquivo.seekg(1 + arquivo.tellg());
		getline(arquivo, comentario);
		arquivo >> largura >> altura;
		arquivo >> maximo;

	}
	else {
		cout << "Não foi possível abrir a imagem" << endl;
	};
}

void Imagem::leitura_imagem(int num){

	arquivo.ignore(maximo, '\n');
	for(int cont = 0; cont < (largura * altura * num); cont++){
		info += arquivo.get();
	}	
}

string Imagem::salvar_copia(){

	ofstream teste;
	teste.open("copia.pgm");
	teste << tipo << endl;
	teste << "#" << comentario << endl;
	teste << largura << " " << altura << endl;
	teste << maximo << endl;
	teste << info;
	teste.close();

	return info;
}
