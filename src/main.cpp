//#include <algorithm>
#include "imagem.hpp"
#include "pgm.hpp"
#include "ppm.hpp"

using namespace std;

int main (int argc, char ** argv){
	
	fstream provisorio;	
	string filename, num_magico;
	cout << "Insira o nome do arquivo com a extensão (Ex.: exemplo.pgm): ";
	cin >> filename;

	provisorio.open(filename);
	provisorio >> num_magico;
	provisorio.close();

	if(num_magico == "P5"){
		PGM * pgm = new PGM(filename);
		if(pgm -> getArquivo()){		
			pgm -> leitura_cabecalho();
			pgm -> leitura_imagem(1);
			pgm -> salvar_copia();
			pgm -> transforma_comentario();
			pgm -> pega_mensagem();
			pgm -> descriptografar();
		
			cout << endl;
			cout << "A mensagem escondida é:  " << pgm -> getMensagem() << endl;	
		}
		else{
			cout << "Erro: Imagem não encontrada." << endl;
		}
	}
	if(num_magico == "P6"){
	
		PPM ppm(filename);

		if(ppm.getArquivo()){		
			ppm.leitura_cabecalho();
			ppm.leitura_imagem(3);
			ppm.salvar_copia();
			ppm.transforma_comentario();
		//	ppm.pega_mensagem();
		//	ppm.descriptografar();
		
			cout << endl;
			cout << "Não consegui descriptografar imagens ppm a tempo, professor..."<< endl;
		}
	}
/*
	else{
		cout << "Erro: Imagem não encontrada." << endl;
	}
	
*/

	return 0;
}
