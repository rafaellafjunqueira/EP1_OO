#include "pgm.hpp"
#include <iostream>

PGM::PGM(){}

PGM::~PGM(){}

PGM::PGM(string filename){
	this -> setArquivo(filename);
}
void PGM::setInicio(int inicio){
	this -> inicio = inicio;
}
int PGM::getInicio(){
	return inicio;
}
void PGM::setTamanho(int tamanho){
	this -> tamanho=tamanho;
}
int PGM::getTamanho(){
	return tamanho;
}
void PGM::setChave(int chave){
	this -> chave=chave;
}
int PGM::getChave(){
	return chave;
}
void PGM::setMensagem(string mensagem){
	this -> mensagem = mensagem;
}
string PGM::getMensagem(){
	return mensagem;
}
void PGM::transforma_comentario(){

	int n = 0, vetor_comentario[3];
	string aux;

	for(int i = 0; i < 3; i++){
		while(comentario[n] != ' '){
			aux += comentario[n];
			n++;		
		}
		vetor_comentario[i] = stoi(aux);
		aux.erase();
		n++;
	}
	inicio = vetor_comentario[0];
	tamanho = vetor_comentario[1];
	chave = vetor_comentario[2]; 
}
void PGM::pega_mensagem(){
			
	for(int j = inicio; j < inicio + tamanho; j++){
		mensagem += info[j];
	}
}

void PGM::descriptografar(){	
	for(int i = 0; i < getTamanho(); i++){
        
		if(mensagem[i] >= 'a' && mensagem[i] <= 'z'){
	            	mensagem[i] = mensagem[i] - getChave();
            
			if(mensagem[i] < 'a'){
				mensagem[i] = mensagem[i] + 'z' - 'a' + 1;
			}
		}
        
		else if(mensagem[i] >= 'A' && mensagem[i] <= 'Z'){
			mensagem[i] = mensagem[i] - getChave();
        	    
			if(mensagem[i] < 'A'){
				mensagem[i] = mensagem[i] + 'Z' - 'A' + 1;
			}
		}
	}
	
}


