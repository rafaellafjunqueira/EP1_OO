#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.hpp"

class PPM : public Imagem {

private:
	int inicio;
	int tamanho;
	int chave;
//	string mensagem;
	
public:
	PPM();
	PPM(string filename);
	~PPM();

	void setInicio(int inicio);
	int getInicio();

	void setTamanho(int tamanho);
	int getTamanho();

	void setChave(int chave);
	int getChave();

	void transforma_comentario();

	void descriptografar();
};

/*
void setInfo(unsigned char * info);
unsigned char getInfo();

void adicionar_char_info(unsigned char infoChar,int posicao){
	this -> info[posicao] = infoChar;
}
unsigned char retornar_char_info(int posicao){
	return info[posicao];

void adicionar_char_info(unsigned char infoChar,int posicao);
unsigned char retornar_char_info(int posicao);
*/

#endif
