#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"

class PGM : public Imagem {

private:
	int inicio;
	int tamanho;
	int chave;
	string mensagem;

public:
	PGM();
	PGM(string filename);
	~PGM();

	void setInicio(int inicio);
	int getInicio();

	void setTamanho(int tamanho);
	int getTamanho();

	void setChave(int chave);
	int getChave();

	void setMensagem(string mensagem);
	string getMensagem();

	void transforma_comentario();

	void pega_mensagem();

	void descriptografar();

};
#endif
