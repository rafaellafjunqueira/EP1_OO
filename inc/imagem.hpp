#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class Imagem{

private:
	ifstream arquivo;
	string tipo;
	int largura;
	int altura;
	int maximo;

protected:
	string info;
	string comentario;

public:
	Imagem();
	Imagem(string filename);
	~Imagem();

	void setArquivo(string filename);
	ifstream& getArquivo();

	void setTipo(string tipo);
	string getTipo();

	void setLargura(int largura);
	int getLargura();

	void setAltura(int altura);
	int getAltura();
	
	void setComentario(string comentario);
	string getComentario();

	void setMaximo(int maximo);
	int getMaximo();

	void leitura_cabecalho();

	void leitura_imagem(int num);

	string salvar_copia();

	void setInfo(string info);

	string getInfo();
	
//	void descriptografar();
		
};
#endif
